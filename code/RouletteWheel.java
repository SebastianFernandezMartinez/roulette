import java.util.Random;
public class RouletteWheel {

    private Random random;
    private int lastSpin;

    public RouletteWheel (){
        this.random = new Random();
        this.lastSpin = 0;
    }

    public void spin(){
        this.lastSpin = this.random.nextInt(37);
    }

    public int getValue(){
        return this.lastSpin;
    }
}

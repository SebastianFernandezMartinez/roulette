import java.util.Scanner;
public class Roulette {

    public static boolean checkIfWon(int number, RouletteWheel roulette){
        if (number == roulette.getValue()){
            return true;
        }else{
            return false;
        }
    }
    // i assume that all the inputs are entered correctly 
    public static void main(String[] args){

        Scanner scan = new Scanner(System.in);
        RouletteWheel roulette  = new RouletteWheel();
        int money = 1000;
        Boolean play;
        String userAnswer;
        int betAmount;
        int betNumber;
        String playAgain;

        System.out.println("Would you like to make a bet?\n y for yes n for no");
        userAnswer = scan.nextLine();
        System.out.println(userAnswer);
        if (userAnswer.contains("y")){
    
            play = true;
        }else{
            play = false;
        }
        while(play){

            System.out.println("How much would you want to bet?");
            betAmount = scan.nextInt();
            while(betAmount>money){
                System.out.println("You are betting more money than you have please give a new number");
                betAmount = scan.nextInt();
            }

            System.out.println("What number would you like to bet on");
            betNumber = scan.nextInt();
            money -= betAmount;

            roulette.spin();

            if(checkIfWon(betNumber, roulette)){

                System.out.println("You have won!");
                money += betAmount*35;
                System.out.println("money = "+money);
            }else{
                System.out.println("you have lost!");
                System.out.println("money = "+money);
            }
            
            System.out.println("would you like to play again? y for yes n for no");
            playAgain = scan.next();
            if(playAgain.contains("n")){
                if(money-1000 > 0){
                    System.out.print("You have made "+(money-1000)+"$" );
                }else {
                    System.out.print("You have lost "+(1000-money)+"$" );
                }
                play = false;
            }
        }
    }
}